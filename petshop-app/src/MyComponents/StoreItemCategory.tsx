import '../App.css'
import { useEffect, useState } from 'react'
import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from '@mui/material'
import { useNavigate } from 'react-router-dom'



type StoreItemCategoryPros = {
  itemCategoryId: number
  itemCategoryName: string
  itemCategoryImage: string
  itemCategoryDescription: string
}


export function StoreItemCategory({
  itemCategoryId,
  itemCategoryName,
  itemCategoryImage,
  itemCategoryDescription
}: StoreItemCategoryPros) {

  const navigate = useNavigate();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const Style: any = {
    width: 300,
  }

  useEffect(() => {
    const token = localStorage.getItem('userEmail');

    if (token) {
      setIsLoggedIn(true);
    }
    else {
      setIsLoggedIn(false);
    }

  })

  const fetchItems = () => {
    if (isLoggedIn) {
      navigate(`/item/${itemCategoryId}`)
    }
    else {
      navigate('/login')
    }

  }

  return (
    <Box className="mainContainer1">
      <Card>
        <CardMedia
          component="img"
          image={require('C:/Users/anuragu/Desktop/DOMO_Project/PetStoreRepo/PetShop/Images/' +
          itemCategoryImage)}
          style={Style}
        />
        <CardContent>
          <Typography align="center" gutterBottom variant="h5" component="div">
            <div className="price">{itemCategoryName}</div>
            <Typography variant="body2" color="text.secondary">
              {itemCategoryDescription}
            </Typography>
            <CardActions>
              <button className='petCategoryShowNow' onClick={() => fetchItems()}>Shop now</button>
            </CardActions>
          </Typography>
        </CardContent>
      </Card>
    </Box>
  )
}
