import { Container, Form, InputGroup } from "react-bootstrap";
import { StoreItem } from "./StoreItem";
import { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

export function Item() {
  const [items, setItems] = useState<any[]>([]);
  const [search, setSearch] = useState("");
  const [token] = useState(sessionStorage.getItem("token_CUSTOMER"));

  type itemCategoryIdType = {
    itemCategoryId: number;
  };

  const { itemCategoryId } = useParams() as any;
  const navigate = useNavigate();

  useEffect(() => {
    if (token == null) {
      navigate("/home");
    }
    axios
      .get(
        `http://localhost:8080/item/getAllItemsByItemCategoryId/${itemCategoryId}`
      )
      .then((response) => {
        setItems(response.data);
      });
  }, []);

  const DisplayData = items
    .filter((item) => {
      return search.toLowerCase() === ""
        ? item
        : item.itemName.toLowerCase().includes(search);
    })

    .map((item) => {
      return (
        <tr key={item.id}>
          <td>
            <StoreItem {...item} />
          </td>
        </tr>
      );
    });
  return (
    <Container>
      <Form>
        <div>
          <h2>he</h2>
        </div>
        <InputGroup className="my-3">
          <Form.Control
            onChange={(e) => setSearch(e.target.value)}
            placeholder="Search"
          />
        </InputGroup>
      </Form>

      <table className="table">
        <tbody className="ShowDogs">{DisplayData}</tbody>
      </table>
    </Container>
  );
}
